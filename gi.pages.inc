<?php

module_load_include("inc", "gi", "gi.functions");


function gi_commit_list_form() {
  $response = gi_call("https://api.github.com/repos/usergrid/docs/commits", "GET");
  $form = array(
	"#type" => "form",
	"#attributes" => array(
		"id" => "gi_commit_list_form"
	)
  );

  $header = array(
	"title" => array( "data" => t("Commit Message")),
	"commiter" => array("data" => t("Committer")),
	"date" => array("data" => t("Date"))
	);
  $options = array();
  foreach($response as $key => $row) {
	$options[$row['sha']] = array(
		"title" =>  check_plain($row['commit']['message']),
		"commiter" =>  $row['commit']['author']['name'],
		"date" =>  $row['commit']['author']['date']
		);
  }
  $form['commits'] = array(
  	"#type" => "tableselect",
	"#header" => $header,
	"#options" => $options,
	"#empty" => t("No Info Available")
  );
	$form['actions'] = array("#type" => "actions");
	$form['actions']['submit'] = array(
	  '#type' => 'submit',
	  '#value' => t("Import Commit(s)"),
	  '#weight' => 10,
	  '#submit' => array("gi_commit_list_submit")
	);
	return $form;
}

function gi_commit_list_submit($form, $form_state) {
	$toReturn = array();
	foreach($form_state['values']['commits'] as $commit) {
		if ($commit != false) {
			// grab commit info, translate it to a node object
			$commit_data = gi_call("https://api.github.com/repos/usergrid/docs/git/commits/".$commit);
			$commit_node = _gi_json_to_node_data_commit($commit_data);
			node_save($commit_node);
			if ($commit_node->nid) {
				echo "Commit Imported::<b>{$commit_node->title}</b> (NID:$commit_node->nid)";
				// grab the commit's root tree info and create a node object
				$tree_data = gi_call($commit_data['tree']['url']);
				$tree_node = _gi_json_to_node_data_tree($tree_data, "Commit Root");
				//save that new node object
				node_save($tree_node);
				// associate newly created tree with commit
				$commit_node->field_tree[LANGUAGE_NONE][0]['nid'] = $tree_node->nid;
				node_save($commit_node);
			}
		}
	}
	drupal_set_message("This(these) commit(s) have been imported.<br>Their Tree(s) will begin importing when the next cron runs.</p><ul><li>" . implode("</li><li>", $toReturn) . "</li></ul>","notice");
	drupal_goto("admin/content/node");
}


<?php



class GIIterator {
	
	public $toImport = array();
	public $trees = array();
	function __construct($initial_tree) {
		$this->trees[] = $initial_tree;
		$this->toImport = $initial_tree->tree;
	}
	
	function getNext() {
		if (count($this->toImport) > 0) {
			return array_shift($this->toImport);
		} else {
			return null;
		}
	}
	function addTree($tree, $pnid = null) {
		$this->trees[] = $tree;
		//if there's a parent node, add it to all the limbs
		if ($pnid != null) {
			foreach($tree->tree as $key => $value) 
				$tree->tree[$key]->pnid = $pnid;
		}
		$this->toImport = array_merge($this->toImport, $tree->tree);
	}
	function addParentRef(&$node, $pnid) {
		
	}
	
	
}
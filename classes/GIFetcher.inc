<?php



class GIFetcherResult extends FeedsFetcherResult {
	
	public function __construct($url) {
    parent::__construct('');
    $this->url = $url;
  }

  /**
   * Overrides parent::getRaw();
   */
  public function getRaw() {
	if(trim($this->url) != "") {
		module_load_include("php", "gi", "classes/CurlMeister");
		$result = CurlMeister::call($this->url, "GET");
		if (!in_array($result['code'], array(200, 201, 202, 203, 204, 205, 206))) {
	      throw new Exception(t('Download of @url failed with code !code.', array('@url' => $this->url, '!code' => $result->code)));
	    }
	    return $this->sanitizeRaw($result['data']);
	} else {
		return false;
	}
  }
	
}

class GIFetcher extends FeedsFetcher {
	
	/**
	   * Implements FeedsFetcher::fetch().
	   */
	  public function fetch(FeedsSource $source) {
	    $source_config = $source->getConfigFor($this);
	    return new FeedsHTTPFetcherResult($source_config['source']);
	  }

	  /**
	   * Clear caches.
	   */
	  public function clear(FeedsSource $source) {
	    $source_config = $source->getConfigFor($this);
	    $url = $source_config['source'];
	    feeds_include_library('http_request.inc', 'http_request');
	    http_request_clear_cache($url);
	  }

	  /**
	   * Implements FeedsFetcher::request().
	   */
	  public function request($feed_nid = 0) {
	    feeds_dbg($_GET);
	    @feeds_dbg(file_get_contents('php://input'));
	    // A subscription verification has been sent, verify.
	    if (isset($_GET['hub_challenge'])) {
	      $this->subscriber($feed_nid)->verifyRequest();
	    }
	    // No subscription notification has ben sent, we are being notified.
	    else {
	      try {
	        feeds_source($this->id, $feed_nid)->existing()->import();
	      }
	      catch (Exception $e) {
	        // In case of an error, respond with a 503 Service (temporary) unavailable.
	        header('HTTP/1.1 503 "Not Found"', NULL, 503);
	        exit();
	      }
	    }
	    // Will generate the default 200 response.
	    header('HTTP/1.1 200 "OK"', NULL, 200);
	    exit();
	  }

	  /**
	   * Override parent::configDefaults().
	   */
	  public function configDefaults() {
	    return array(
	      'auto_detect_feeds' => FALSE,
	      'use_pubsubhubbub' => FALSE,
	      'designated_hub' => '',
	    );
	  }

	  /**
	   * Override parent::configForm().
	   */
	  public function configForm(&$form_state) {
	    $form = array();
	    $form['github_user'] = array(
	      '#type' => 'textfield',
	      '#title' => t('Github User ID'),
	      '#description' => t('The User ID owner of the Repo for import'),
	      '#default_value' => $this->config['github_user'],
	    );
	    $form['github_repo'] = array(
	      '#type' => 'textfield',
	      '#title' => t('Name of Repo'),
	      '#description' => t('Name of the repo that will be imported'),
	      '#default_value' => $this->config['github_repo'],
	    );
	    return $form;
	  }

	  /**
	   * Expose source form.
	   */
	  public function sourceForm($source_config) {
	    $form = array();
	    $form['commit_sha'] = array(
	      '#type' => 'textfield',
	      '#title' => t('Commit ID'),
	      '#description' => t('Leave blank for Most Recent commit to master.'),
	      '#default_value' => isset($source_config['commit_sha']) ? $source_config['commit_sha'] : '',
	      '#maxlength' => NULL,
	      '#required' => TRUE,
	    );
	    return $form;
	  }

	  /**
	   * Override parent::sourceFormValidate().
	   */
	  public function sourceFormValidate(&$values) {


	  }

	  /**
	   * Override sourceSave() - subscribe to hub.
	   */
	  public function sourceSave(FeedsSource $source) {


	  }

	  /**
	   * Override sourceDelete() - unsubscribe from hub.
	   */
	  public function sourceDelete(FeedsSource $source) {

	  }

	  /**
	   * Implement FeedsFetcher::subscribe() - subscribe to hub.
	   */
	  public function subscribe(FeedsSource $source) {
	    $source_config = $source->getConfigFor($this);
	    $this->subscriber($source->feed_nid)->subscribe($source_config['source'], url($this->path($source->feed_nid), array('absolute' => TRUE)), valid_url($this->config['designated_hub']) ? $this->config['designated_hub'] : '');
	  }

	  /**
	   * Implement FeedsFetcher::unsubscribe() - unsubscribe from hub.
	   */
	  public function unsubscribe(FeedsSource $source) {
	    $source_config = $source->getConfigFor($this);
	    $this->subscriber($source->feed_nid)->unsubscribe($source_config['source'], url($this->path($source->feed_nid), array('absolute' => TRUE)));
	  }

	  /**
	   * Implement FeedsFetcher::importPeriod().
	   */
	  public function importPeriod(FeedsSource $source) {
	    if ($this->subscriber($source->feed_nid)->subscribed()) {
	      return 259200; // Delay for three days if there is a successful subscription.
	    }
	  }

	  /**
	   * Convenience method for instantiating a subscriber object.
	   */
	  protected function subscriber($subscriber_id) {
	    return PushSubscriber::instance($this->id, $subscriber_id, 'PuSHSubscription', PuSHEnvironment::instance());
	  }
	
	
	
}